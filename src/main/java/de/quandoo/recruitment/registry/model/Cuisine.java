package de.quandoo.recruitment.registry.model;

import com.google.common.base.Objects;

public class Cuisine {

  private final String name;

  public Cuisine(final String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Cuisine cuisine = (Cuisine) o;
    return Objects.equal(name, cuisine.name);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(name);
  }

}
