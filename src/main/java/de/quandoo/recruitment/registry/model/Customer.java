package de.quandoo.recruitment.registry.model;

import com.google.common.base.Objects;

public class Customer {

    private final String uuid;

    public Customer(final String uuid) {
        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equal(uuid, customer.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(uuid);
    }


    public String getUuid() {
        return uuid;

    }
}
