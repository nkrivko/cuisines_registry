package de.quandoo.recruitment.registry;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.MultimapBuilder;
import com.google.common.collect.Multimaps;
import com.google.common.collect.SetMultimap;
import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import de.quandoo.recruitment.registry.annotations.ThreadSafe;
import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.locks.StampedLock;
import java.util.function.Function;
import java.util.stream.Collectors;

@ThreadSafe
public class AdvancedInMemoryCuisinesRegistry implements CuisinesRegistry {

  private final SetMultimap<Customer, Cuisine> storage = MultimapBuilder.<Customer, Cuisine>hashKeys().hashSetValues().build();
  private final BloomFilter<String> cuisineBloomFilter = BloomFilter.create(Funnels.stringFunnel(StandardCharsets.UTF_8), 1000000, 0.001);
  private final StampedLock lock = new StampedLock();

  @Override
  public void register(final Customer customer, final Cuisine cuisine) {
    long stamp = lock.writeLock();
    try {
      storage.put(customer, cuisine);
      cuisineBloomFilter.put(cuisine.getName());
    } finally {
      lock.unlockWrite(stamp);
    }
  }

  @Override
  public List<Cuisine> customerCuisines(final Customer customer) {
    long stamp = lock.tryOptimisticRead();
    final Set<Cuisine> cuisineSet = storage.get(customer);
    if (lock.validate(stamp)) return Lists.newArrayList(cuisineSet);
    stamp = lock.readLock();
    try {
      final Set<Cuisine> cuisineSetAgain = storage.get(customer);
      return Lists.newArrayList(cuisineSetAgain);
    } finally {
      lock.unlock(stamp);
    }
  }

  @Override
  public List<Cuisine> topCuisines(int n) {
    long stamp = lock.tryOptimisticRead();
    final Collection<Cuisine> values = storage.values();
    if (lock.validate(stamp)) {
      return this.topOf(n, values);
    }
    stamp = lock.readLock();
    try {
      final Collection<Cuisine> valuesAgain = storage.values();
      return this.topOf(n, valuesAgain);
    } finally {
      lock.unlock(stamp);
    }
  }

  private List<Cuisine> topOf(final int n, final Collection<Cuisine> values) {
    final Map<Cuisine, Long> groupedValues = values.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    return groupedValues.entrySet().stream().sorted(Comparator.comparing(Entry<Cuisine, Long>::getValue).reversed())
      .limit(n).map(Entry::getKey).collect(Collectors.toList());
  }

  @Override
  public List<Customer> cuisineCustomers(final Cuisine cuisine) {
    if (cuisine == null) return ImmutableList.of();
    long stamp = lock.tryOptimisticRead();
    final List<Customer> customers = this.customers(cuisine);
    if (lock.validate(stamp)) {
      return customers;
    }
    stamp = lock.readLock();
    try {
      return this.customers(cuisine);
    } finally {
      lock.unlock(stamp);
    }
  }

  private List<Customer> customers(Cuisine cuisine) {
    final boolean contain = cuisineBloomFilter.mightContain(cuisine.getName());
    if (!contain) {
      return ImmutableList.of();
    }
    final ListMultimap<Cuisine, Customer> destination = MultimapBuilder.<Customer, Cuisine>hashKeys()
      .arrayListValues().build();
    final ListMultimap<Cuisine, Customer> result = Multimaps.invertFrom(storage, destination);
    return result.get(cuisine);
  }
}
