package de.quandoo.recruitment.registry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.google.common.collect.Lists;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AdvancedInMemoryCuisinesTest {

  private final AdvancedInMemoryCuisinesRegistry cuisinesRegistry = new AdvancedInMemoryCuisinesRegistry();

  @Before
  public void setUp() throws Exception {
    cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
    cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
    cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));
    cuisinesRegistry.register(new Customer("4"), new Cuisine("italian"));
    cuisinesRegistry.register(new Customer("5"), new Cuisine("italian"));
    cuisinesRegistry.register(new Customer("6"), new Cuisine("italian"));
    cuisinesRegistry.register(new Customer("6"), new Cuisine("french"));
  }

  @Test
  public void shouldHaveOneFrench() {
    Customer customer = new Customer("1");
    Cuisine french = new Cuisine("french");
    assertTrue(cuisinesRegistry.customerCuisines(customer).contains(french));
  }

  @Test
  public void shouldWorkReturnEmptyCustomerCollection() {
    List<Customer> customers = cuisinesRegistry.cuisineCustomers(null);
    assertTrue(customers.isEmpty());
  }

  @Test
  public void shouldReturnEmptyCuisineCollection() {
    List<Cuisine> cuisines = cuisinesRegistry.customerCuisines(null);
    assertTrue(cuisines.isEmpty());
  }

  @Test
  public void shouldReturnCollectionWithOneElm() {
    List<Cuisine> cuisines = cuisinesRegistry.topCuisines(1);
    assertTrue(cuisines.size() == 1);
    Cuisine cuisine = cuisines.get(0);
    assertEquals("italian", cuisine.getName());
  }

  @Test
  public void shouldReturnListOfTwoTopCuisines() {
    List<Cuisine> cuisines = cuisinesRegistry.topCuisines(2);
    assertTrue(!cuisines.isEmpty());
    Cuisine cuisine1 = cuisines.get(0);
    Cuisine cuisine2 = cuisines.get(1);
    assertEquals("italian", cuisine1.getName());
    assertEquals("french", cuisine2.getName());
  }

  @Test
  public void shouldReturnAllCustomersWithItalianCuisine() {
    Cuisine italian = new Cuisine("italian");
    List<Customer> customers = cuisinesRegistry.cuisineCustomers(italian);
    assertTrue(customers.size() == 4);
    final ArrayList<String> expectedUids = Lists.newArrayList("3", "4", "5", "6");
    customers.stream().forEach((customer) -> {
      String uuid = customer.getUuid();
      Assert.assertTrue(expectedUids.contains(uuid));
    });
  }

  @Test
  public void shouldReturnAllCuisineCollectionForCustomerUid6() {
    List<Cuisine> cuisines = cuisinesRegistry.customerCuisines(new Customer("6"));
    assertTrue(!cuisines.isEmpty());
    assertTrue(cuisines.size()==2);
    final ArrayList<String> expectedNames= Lists.newArrayList("italian","french");
    cuisines.stream().forEach((cuisine -> {
      String name = cuisine.getName();
      Assert.assertTrue(expectedNames.contains(name));
    }));
  }


}
