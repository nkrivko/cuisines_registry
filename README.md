# Cuisines Registry

## The story:

Cuisines Registry is an important part of Book-That-Table Inc. backend stack. It keeps in memory customer preferences for restaurant cuisines and is accessed by a bunch of components to register and retrieve data. 


The first iteration of this component was implemented by rather inexperienced developer and now may require some cleaning while new functionality is being added. But fortunately, according to his words: "Everything should work and please keep the test coverage as high as I did"


## Your tasks:
1. **[Important!]** Adhere to the boy scout rule. Leave your code better than you found it.
It is ok to change any code as long as the CuisinesRegistry interface remains unchanged.
2. Make is possible for customers to follow more than one cuisine (return multiple cuisines in de.quandoo.recruitment.registry.api.CuisinesRegistry#customerCuisines)
3. Implement de.quandoo.recruitment.registry.api.CuisinesRegistry#topCuisines - returning list of most popular (highest number of registered customers) ones
4. Create a short write up on how you would plan to scale this component to be able process in-memory billions of customers and millions of cuisines (Book-That-Table is already planning for galactic scale). (100 words max)

## Submitting your solution

+ Fork it to a private gitlab repository.
+ Put write up mentioned in point 4. into this file.
+ Send us a link to the repository, together with private ssh key that allows access (settings > repository > deploy keys).

##Scaling

We can deploy many nodes of CuisineRegistry instances and split responsibility by range of keys. 
Also we need to component implementation, standing before Cuisine Registry nodes,  
which is responsible for sending requests by keys to corresponding nodes.
Example, 
node1: uids 1…100 000, 
node2: uids 100 001…200 000,
node3: uids 200 001…300 000
etc

For customerCuisines requests, we need to build bitmap in each node for each cuisine.  
Example: If the Italian cuisine has 01011101 => Customers with ids: 2,4,5,6,8 has Italian cuisine.

For top of n cuisine we need just to compare such bitmasks (and store bitmasks to any kind of sortedSet).
Example of comparison: 
00101>10000
11000=00101
10101<11111 



